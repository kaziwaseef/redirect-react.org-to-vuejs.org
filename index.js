// Add the following lines to /etc/hosts
// 127.0.0.1	reactjs.org

var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('new.cert.key', 'utf8');
var certificate = fs.readFileSync('new.cert.cert', 'utf8');

var credentials = {key: privateKey, cert: certificate};
var express = require('express');
var app = express();

// your express configuration here

app.get('/', (req, res) => res.redirect('https://vuejs.org'));

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(80);
httpsServer.listen(443);